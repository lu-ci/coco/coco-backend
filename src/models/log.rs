use crate::core::server::CocoServer;
use bson::{doc, Document};
use mongodb::options::FindOptions;
use serde::{Deserialize, Serialize};
use serenity::futures::TryStreamExt;

pub const MODEL_COLL: &str = "logs";

#[derive(Deserialize, Serialize)]
pub struct CocoLog {
    pub user: i64,
    pub pair: i64,
    pub message: String,
    pub timestamp: i64,
}

impl CocoLog {
    pub async fn create(
        srv: &CocoServer,
        user: i64,
        pair: i64,
        message: impl ToString,
    ) -> anyhow::Result<Self> {
        let message = message.to_string();
        let coll = srv.db.coll(MODEL_COLL);
        let timestamp = chrono::Utc::now().timestamp();
        let log = Self {
            user,
            pair,
            message,
            timestamp,
        };
        let doc = bson::to_bson(&log)?.as_document().unwrap().clone();
        let _ = coll.insert_one(doc, None).await?;
        Ok(log)
    }

    pub async fn find(srv: &CocoServer, pair: i64) -> anyhow::Result<Vec<Self>> {
        let mut logs = Vec::new();
        let coll = srv.db.coll(MODEL_COLL);
        let lookup = doc! {"pair": pair};
        let opts = FindOptions::builder().sort(doc! {"timestamp": -1}).build();
        let cursor = coll.find(lookup, opts).await?;
        let results = cursor.try_collect::<Vec<Document>>().await?;
        for result in results {
            let log = bson::from_document::<Self>(result)?;
            logs.push(log);
        }
        Ok(logs)
    }
}
