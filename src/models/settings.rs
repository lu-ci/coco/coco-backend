use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoRatings {
    #[serde(default = "bool::default")]
    pub safe: bool,
    #[serde(default = "bool::default")]
    pub questionable: bool,
    #[serde(default = "bool::default")]
    pub explicit: bool,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoTags {
    #[serde(default = "Vec::<String>::default")]
    pub nsfw: Vec<String>,
    #[serde(default = "Vec::<String>::default")]
    pub exclude: Vec<String>,
    #[serde(default = "Vec::<String>::default")]
    pub require: Vec<String>,
    #[serde(default = "CocoRatings::default")]
    pub ratings: CocoRatings,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoSites {
    #[serde(default = "bool::default")]
    pub yandere: bool,
    #[serde(default = "bool::default")]
    pub konachan: bool,
    #[serde(default = "bool::default")]
    pub safebooru: bool,
    #[serde(default = "bool::default")]
    pub reddit: bool,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoSources {
    #[serde(default = "CocoSites::default")]
    pub sites: CocoSites,
    #[serde(default = "Vec::<String>::default")]
    pub subs: Vec<String>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoMargins {
    #[serde(default = "i64::default")]
    pub age: i64,
    #[serde(default = "i64::default")]
    pub score: i64,
    #[serde(default = "bool::default")]
    pub active: bool,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CocoSettings {
    #[serde(default = "CocoTags::default")]
    pub tags: CocoTags,
    #[serde(default = "CocoSources::default")]
    pub sources: CocoSources,
    #[serde(default = "CocoMargins::default")]
    pub margins: CocoMargins,
    #[serde(default = "bool::default")]
    pub active: bool,
}
