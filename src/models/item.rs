use bson::doc;
use serde::{Deserialize, Serialize};

use crate::core::database::CocoDatabase;

const MODEL_COLL: &str = "posts";

#[derive(Clone, Deserialize, Serialize)]
pub struct CocoItem {
    pub id: i64,
    pub md5: String,
    pub url: String,
    pub file: String,
    pub tags: Vec<String>,
    pub score: i64,
    pub source: String,
    #[serde(default = "Option::<String>::default")]
    pub source_name: Option<String>,
    pub active: bool,
    pub rating: i64,
    pub timestamp: i64,
}

impl CocoItem {
    pub async fn find(db: &CocoDatabase, id: i64, source: &str) -> anyhow::Result<Option<Self>> {
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"id": id, "source": source};
        Ok(if let Some(doc) = coll.find_one(search, None).await? {
            Some(bson::from_document::<Self>(doc)?)
        } else {
            None
        })
    }
}
