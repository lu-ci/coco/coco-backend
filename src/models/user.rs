use crate::core::server::CocoServer;
use bson::doc;
use serde::{Deserialize, Serialize};

const MODEL_COLL: &str = "users";

#[derive(Clone, Deserialize, Serialize)]
pub struct CocoUser {
    pub id: String,
    pub username: String,
    pub avatar: String,
    pub discriminator: String,
}

impl CocoUser {
    pub fn id(&self) -> i64 {
        self.id.parse::<i64>().unwrap()
    }

    pub async fn save(&self, srv: &CocoServer) -> anyhow::Result<()> {
        let coll = srv.db.coll(MODEL_COLL);
        let search = doc! {"id": &self.id};
        let data = doc! {"$set": bson::to_bson(&self)?.as_document().unwrap().clone() };
        let opts: mongodb::options::UpdateOptions = mongodb::options::UpdateOptions::builder()
            .upsert(true)
            .build();
        let _ = coll.update_one(search, data, opts).await?;
        Ok(())
    }

    pub async fn get(srv: &CocoServer, id: impl ToString) -> anyhow::Result<Option<Self>> {
        let search = doc! {"id": id.to_string()};
        Ok(
            if let Some(result) = srv.db.coll(MODEL_COLL).find_one(search, None).await? {
                Some(bson::from_document::<Self>(result)?)
            } else {
                None
            },
        )
    }
}
