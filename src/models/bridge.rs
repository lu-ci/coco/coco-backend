use crate::core::server::CocoServer;
use bson::{doc, Document};
use mongodb::options::FindOneOptions;
use serde::{Deserialize, Serialize};
use serenity::futures::TryStreamExt;

pub const MODEL_COLL: &str = "bridges";

#[derive(Deserialize, Serialize)]
pub struct CocoBridge {
    pub id: i64,
    pub user: i64,
    pub pair: i64,
}

impl CocoBridge {
    pub async fn create(srv: &CocoServer, user: i64, pair: i64) -> anyhow::Result<Self> {
        let coll = srv.db.coll(MODEL_COLL);
        let opts = FindOneOptions::builder().sort(doc! {"id": -1}).build();
        let id = if let Some(res) = coll.find_one(None, opts).await? {
            let pair = bson::from_document::<Self>(res)?;
            pair.id + 1
        } else {
            0
        };
        let bridge = Self { id, user, pair };
        let doc = bson::to_bson(&bridge)?.as_document().unwrap().clone();
        let _ = coll.insert_one(doc, None).await?;
        Ok(bridge)
    }

    pub async fn find(srv: &CocoServer, user: i64) -> anyhow::Result<Vec<Self>> {
        let mut bridges = Vec::new();
        let coll = srv.db.coll(MODEL_COLL);
        let lookup = doc! {"user": user};
        let cursor = coll.find(lookup, None).await?;
        let results = cursor.try_collect::<Vec<Document>>().await?;
        for result in results {
            let bridge = bson::from_document::<Self>(result)?;
            bridges.push(bridge);
        }
        Ok(bridges)
    }
}
