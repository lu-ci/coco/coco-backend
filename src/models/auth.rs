use bson::doc;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::core::server::CocoServer;
use crate::models::user::CocoUser;

const MODEL_COLL: &str = "auth";
const OAUTH_BASE: &str = "https://discord.com/api/oauth2";

#[derive(Deserialize)]
pub struct AccessTokenResponse {
    pub access_token: String,
    pub token_type: String,
    pub expires_in: i64,
    pub scope: String,
}

#[derive(Deserialize)]
pub struct AuthInfoResponse {
    pub user: CocoUser,
}

#[derive(Deserialize, Serialize)]
pub struct CocoAuth {
    pub id: i64,
    pub token: String,
    pub bearer: String,
    pub expiration: i64,
}

impl CocoAuth {
    pub async fn oauth(
        srv: &CocoServer,
        token: impl ToString,
        code: impl ToString,
    ) -> anyhow::Result<Self> {
        let token = token.to_string();
        let code = code.to_string();
        let cli = reqwest::Client::new();
        let mut form = HashMap::new();
        form.insert("client_id", srv.cfg.dsc.id.to_string());
        form.insert("client_secret", srv.cfg.dsc.secret.clone());
        form.insert("grant_type", "authorization_code".to_string());
        form.insert("code", code);
        form.insert("redirect_uri", srv.cfg.core.redirect.clone());
        let resp = cli
            .post(format!("{}/token", OAUTH_BASE))
            .form(&form)
            .send()
            .await?;
        let body = resp.text().await?;
        let exch = serde_json::from_str::<AccessTokenResponse>(&body)?;
        let expiration = chrono::Utc::now().timestamp() + exch.expires_in;
        let resp = cli
            .get(format!("{}/@me", OAUTH_BASE))
            .bearer_auth(&exch.access_token)
            .send()
            .await?;
        let body = resp.text().await?;
        let info = serde_json::from_str::<AuthInfoResponse>(&body)?;
        info.user.save(srv).await?;
        let auth = Self {
            id: info.user.id(),
            token,
            bearer: exch.access_token,
            expiration,
        };
        auth.save(srv).await?;
        Ok(auth)
    }

    pub async fn save(&self, srv: &CocoServer) -> anyhow::Result<()> {
        let coll = srv.db.coll(MODEL_COLL);
        let search = doc! {"id": self.id as i64};
        let data = doc! {"$set": bson::to_bson(&self)?.as_document().unwrap().clone() };
        let opts: mongodb::options::UpdateOptions = mongodb::options::UpdateOptions::builder()
            .upsert(true)
            .build();
        let _ = coll.update_one(search, data, opts).await?;
        Ok(())
    }

    pub async fn get(srv: &CocoServer, token: impl ToString) -> anyhow::Result<Option<Self>> {
        let search = doc! {"token": token.to_string()};
        Ok(
            if let Some(result) = srv.db.coll(MODEL_COLL).find_one(search, None).await? {
                Some(bson::from_document::<Self>(result)?)
            } else {
                None
            },
        )
    }
}
