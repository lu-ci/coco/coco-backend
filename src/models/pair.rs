use serde::{Deserialize, Serialize};

use crate::core::server::CocoServer;
use crate::models::bridge::{CocoBridge, MODEL_COLL as BRIDGE_MODEL_COLL};
use crate::models::channel::{CocoChannel, CocoChannelRequest, CocoChannelResponse};
use crate::models::log::MODEL_COLL as LOG_MODEL_COLL;
use crate::models::settings::CocoSettings;
use bson::doc;
use mongodb::options::FindOneOptions;

const MODEL_COLL: &str = "pairs";

#[derive(Deserialize, Serialize)]
pub struct CocoChannelPair {
    pub id: i64,
    pub sfw: Option<i64>,
    pub nsfw: Option<i64>,
    pub name: String,
    pub settings: Option<CocoSettings>,
}

impl CocoChannelPair {
    pub async fn create(srv: &CocoServer) -> anyhow::Result<Self> {
        let coll = srv.db.coll(MODEL_COLL);
        let opts = FindOneOptions::builder().sort(doc! {"id": -1}).build();
        let id = if let Some(res) = coll.find_one(None, opts).await? {
            let pair = bson::from_document::<Self>(res)?;
            pair.id + 1
        } else {
            0
        };
        let pair = Self {
            id,
            sfw: None,
            nsfw: None,
            name: format!("Channel Pair #{}", id),
            settings: None,
        };
        let doc = bson::to_bson(&pair)?.as_document().unwrap().clone();
        let _ = coll.insert_one(doc, None).await?;
        Ok(pair)
    }

    pub async fn find(srv: &CocoServer, user: i64) -> anyhow::Result<Vec<Self>> {
        let coll = srv.db.coll(MODEL_COLL);
        let mut pairs = Vec::new();
        let bridges = CocoBridge::find(srv, user).await?;
        for bridge in bridges {
            let lookup = doc! {"id": bridge.pair};
            let pair = coll.find_one(lookup, None).await?;
            if let Some(pair) = pair {
                let pair = bson::from_document::<Self>(pair)?;
                pairs.push(pair);
            }
        }
        Ok(pairs)
    }

    pub async fn delete(srv: &CocoServer, user: i64, pair: i64) -> anyhow::Result<Option<Self>> {
        let coll = srv.db.coll(MODEL_COLL);
        let mut res = None;
        let bridge_lookup = doc! {"user": user, "pair": pair};
        let pair_lookup = doc! {"id": pair};
        let log_lookup = doc! {"pair": pair};
        if srv
            .db
            .coll(BRIDGE_MODEL_COLL)
            .find_one(bridge_lookup.clone(), None)
            .await?
            .is_some()
        {
            if let Some(pair_doc) = coll.find_one(pair_lookup.clone(), None).await? {
                let pair = bson::from_document::<Self>(pair_doc)?;
                res = Some(pair);
                let _ = coll.delete_one(pair_lookup.clone(), None).await?;
                let _ = srv
                    .db
                    .coll(BRIDGE_MODEL_COLL)
                    .delete_one(bridge_lookup, None)
                    .await?;
                let _ = srv
                    .db
                    .coll(LOG_MODEL_COLL)
                    .delete_many(log_lookup, None)
                    .await?;
            }
        }
        Ok(res)
    }

    pub async fn get(srv: &CocoServer, user: i64, pair: i64) -> anyhow::Result<Option<Self>> {
        let coll = srv.db.coll(MODEL_COLL);
        let mut res = None;
        let bridge_lookup = doc! {"user": user, "pair": pair};
        let pair_lookup = doc! {"id": pair};
        if srv
            .db
            .coll(BRIDGE_MODEL_COLL)
            .find_one(bridge_lookup.clone(), None)
            .await?
            .is_some()
        {
            if let Some(pair_doc) = coll.find_one(pair_lookup.clone(), None).await? {
                res = Some(bson::from_document::<Self>(pair_doc)?);
            }
        }
        Ok(res)
    }

    pub async fn save(&self, srv: &CocoServer) -> anyhow::Result<()> {
        let coll = srv.db.coll(MODEL_COLL);
        let search = doc! {"id": &self.id};
        let data = doc! {"$set": bson::to_bson(&self)?.as_document().unwrap().clone() };
        let opts: mongodb::options::UpdateOptions = mongodb::options::UpdateOptions::builder()
            .upsert(true)
            .build();
        let _ = coll.update_one(search, data, opts).await?;
        Ok(())
    }
}

impl From<CocoChannelPairResponse> for CocoChannelPair {
    fn from(res: CocoChannelPairResponse) -> Self {
        Self {
            id: res.id,
            sfw: res.sfw.as_ref().map(|sfw| sfw.id),
            nsfw: res.nsfw.as_ref().map(|nsfw| nsfw.id),
            name: res.name.clone(),
            settings: res.settings,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CocoChannelPairResponse {
    pub id: i64,
    pub sfw: Option<CocoChannelResponse>,
    pub nsfw: Option<CocoChannelResponse>,
    pub name: String,
    pub settings: Option<CocoSettings>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CocoChanelPairRequest {
    pub id: i64,
    pub sfw: Option<CocoChannelRequest>,
    pub nsfw: Option<CocoChannelRequest>,
    pub name: String,
    pub settings: Option<CocoSettings>,
}

impl CocoChanelPairRequest {
    pub fn to_normal(&self) -> anyhow::Result<CocoChannelPairResponse> {
        Ok(CocoChannelPairResponse {
            id: self.id,
            sfw: if let Some(sfw) = &self.sfw {
                Some(sfw.to_normal()?)
            } else {
                None
            },
            nsfw: if let Some(nsfw) = &self.nsfw {
                Some(nsfw.to_normal()?)
            } else {
                None
            },
            name: self.name.clone(),
            settings: self.settings.clone(),
        })
    }
}

impl CocoChannelPairResponse {
    async fn fetch_channel_response(
        srv: &CocoServer,
        copt: Option<i64>,
    ) -> anyhow::Result<Option<CocoChannelResponse>> {
        Ok(if let Some(cid) = copt {
            CocoChannel::get(srv, cid)
                .await?
                .map(CocoChannelResponse::from)
        } else {
            None
        })
    }

    pub async fn from_pair(srv: &CocoServer, pair: &CocoChannelPair) -> anyhow::Result<Self> {
        let id = pair.id;
        let sfw = Self::fetch_channel_response(srv, pair.sfw).await?;
        let nsfw = Self::fetch_channel_response(srv, pair.nsfw).await?;
        let name = pair.name.clone();
        let settings = pair.settings.clone();
        Ok(Self {
            id,
            sfw,
            nsfw,
            name,
            settings,
        })
    }
}
