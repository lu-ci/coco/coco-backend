use anyhow::anyhow;
use bson::doc;
use serde::{Deserialize, Serialize};
use serenity::http::Http;

use crate::core::server::CocoServer;
use crate::models::pair::CocoChannelPair;

const MODEL_COLL: &str = "channels";
const DISCORD_BASE: &str = "https://discord.com/api/webhooks";

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct CocoChannel {
    pub id: i64,
    pub token: String,
}

impl CocoChannel {
    pub fn from_uri(uri: &str) -> anyhow::Result<Self> {
        let pieces = uri.split('/').collect::<Vec<&str>>();
        if pieces.len() >= 2 {
            let token = pieces.last().unwrap();
            let id = pieces.get(pieces.len() - 2).unwrap();
            let id = id.to_string().parse::<i64>()?;
            let token = token.to_string();
            Ok(Self { id, token })
        } else {
            Err(anyhow!("Missing enough pieces."))
        }
    }

    pub async fn validate(&self) -> anyhow::Result<()> {
        let http = Http::default();
        let _ = http
            .get_webhook_with_token(self.id as u64, &self.token)
            .await?;
        Ok(())
    }

    pub async fn _bound(&self, srv: &CocoServer, pair: &CocoChannelPair) -> anyhow::Result<bool> {
        Ok(if let Some(stored) = Self::get(srv, self.id).await? {
            let sfw_bound = if let Some(sfw) = pair.sfw {
                stored.id == sfw
            } else {
                false
            };
            let nsfw_bound = if let Some(nsfw) = pair.nsfw {
                stored.id == nsfw
            } else {
                false
            };
            !(sfw_bound || nsfw_bound)
        } else {
            false
        })
    }

    pub async fn test(&self) -> anyhow::Result<()> {
        let http = Http::default();
        let hook = http
            .get_webhook_with_token(self.id as u64, &self.token)
            .await?;
        let _ = hook
            .execute(http, true, |m| {
                m.content("**Coco** webhook test.");
                m
            })
            .await?;
        Ok(())
    }

    pub async fn get(srv: &CocoServer, id: i64) -> anyhow::Result<Option<Self>> {
        let coll = srv.db.coll(MODEL_COLL);
        Ok(
            if let Some(doc) = coll.find_one(doc! {"id": id}, None).await? {
                Some(bson::from_document::<Self>(doc)?)
            } else {
                None
            },
        )
    }

    pub async fn save(&self, srv: &CocoServer) -> anyhow::Result<()> {
        let coll = srv.db.coll(MODEL_COLL);
        let search = doc! {"id": &self.id};
        let data = doc! {"$set": bson::to_bson(&self)?.as_document().unwrap().clone() };
        let opts: mongodb::options::UpdateOptions = mongodb::options::UpdateOptions::builder()
            .upsert(true)
            .build();
        let _ = coll.update_one(search, data, opts).await?;
        Ok(())
    }

    pub async fn delete(&self, srv: &CocoServer) -> anyhow::Result<()> {
        let coll = srv.db.coll(MODEL_COLL);
        let search = doc! {"id": &self.id};
        let _ = coll.delete_one(search, None).await?;
        Ok(())
    }
}

impl From<CocoChannelResponse> for CocoChannel {
    fn from(chn: CocoChannelResponse) -> Self {
        Self {
            id: chn.id,
            token: chn.token,
        }
    }
}

impl From<&CocoChannelResponse> for CocoChannel {
    fn from(chn: &CocoChannelResponse) -> Self {
        Self {
            id: chn.id,
            token: chn.token.clone(),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CocoChannelResponse {
    pub id: i64,
    pub token: String,
    pub full: String,
}

impl From<CocoChannel> for CocoChannelResponse {
    fn from(chn: CocoChannel) -> Self {
        Self {
            id: chn.id,
            token: chn.token.clone(),
            full: format!("{}/{}/{}", DISCORD_BASE, chn.id, chn.token),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CocoChannelRequest {
    pub id: String,
    pub token: String,
    pub full: String,
}

impl CocoChannelRequest {
    pub fn to_normal(&self) -> anyhow::Result<CocoChannelResponse> {
        Ok(CocoChannelResponse {
            id: self.id.parse::<i64>()?,
            token: self.token.clone(),
            full: self.full.clone(),
        })
    }
}
