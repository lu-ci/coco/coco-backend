use crate::core::database::CocoDatabase;
use bson::{doc, Document};
use mongodb::options::FindOptions;
use serde::{Deserialize, Serialize};
use serenity::futures::TryStreamExt;

pub const MODEL_COLL: &str = "tracks";

#[derive(Deserialize, Serialize)]
pub struct CocoTrack {
    pub post: i64,
    pub pair: i64,
    pub hash: String,
    pub service: String,
    pub timestamp: i64,
}

impl CocoTrack {
    pub async fn find(db: &CocoDatabase, pair: i64) -> anyhow::Result<Vec<Self>> {
        let mut tracks = Vec::new();
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"pair": pair};
        let opts = FindOptions::builder()
            .sort(doc! {"timestamp": -1})
            .limit(100)
            .build();
        let cursor = coll.find(search, opts).await?;
        let results = cursor.try_collect::<Vec<Document>>().await?;
        for result in results {
            let track = bson::from_document::<Self>(result)?;
            tracks.push(track);
        }
        Ok(tracks)
    }
}
