use serde::Serialize;
use warp::{Rejection, Reply};

use crate::core::database::CocoDatabase;
use crate::core::server::CocoServer;
use crate::models::auth::CocoAuth;
use crate::models::item::CocoItem;
use crate::models::pair::CocoChannelPair;
use crate::models::track::CocoTrack;
use crate::models::user::CocoUser;

#[derive(Serialize)]
pub struct CocoTrackResponse {
    pub post: Option<CocoItem>,
    pub hash: String,
    pub service: String,
    pub timestamp: i64,
}

impl CocoTrackResponse {
    pub async fn new(db: &CocoDatabase, raw: CocoTrack) -> anyhow::Result<Self> {
        let item = CocoItem::find(db, raw.post, &raw.service).await?;
        Ok(Self {
            post: item,
            hash: raw.hash,
            service: raw.service,
            timestamp: raw.timestamp,
        })
    }
}

async fn fetch_tracks(srv: &CocoServer, pair: i64) -> anyhow::Result<Vec<CocoTrackResponse>> {
    let mut tracks = Vec::new();
    for raw in CocoTrack::find(&srv.db, pair).await? {
        let track = CocoTrackResponse::new(&srv.db, raw).await?;
        tracks.push(track);
    }
    Ok(tracks)
}

async fn offloader(
    srv: &CocoServer,
    token: impl ToString,
    pair: i64,
) -> anyhow::Result<warp::http::Response<String>> {
    Ok(if let Some(auth) = CocoAuth::get(srv, token).await? {
        if let Some(user) = CocoUser::get(srv, auth.id).await? {
            if let Some(pair) = CocoChannelPair::get(srv, user.id(), pair).await? {
                let tracks = fetch_tracks(srv, pair.id).await?;
                warp::http::Response::builder()
                    .status(200)
                    .header("Content-Type", "application/json")
                    .body(serde_json::to_string(&tracks)?)?
            } else {
                warp::http::Response::builder()
                    .status(404)
                    .body("Channel pair not found.".to_string())?
            }
        } else {
            warp::http::Response::builder()
                .status(403)
                .body("Forbidden.".to_string())?
        }
    } else {
        warp::http::Response::builder()
            .status(401)
            .body("Unauthorized.".to_string())?
    })
}

pub async fn track_list_handler(
    srv: CocoServer,
    token: String,
    pair: i64,
) -> Result<impl Reply, Rejection> {
    Ok(match offloader(&srv, token, pair).await {
        Ok(reply) => reply,
        Err(err) => warp::http::Response::builder()
            .status(500)
            .body(format!("Error: {}", err))
            .unwrap(),
    })
}
