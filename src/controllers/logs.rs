use serde::Serialize;
use warp::{Rejection, Reply};

use crate::core::server::CocoServer;
use crate::models::auth::CocoAuth;
use crate::models::log::CocoLog;
use crate::models::pair::CocoChannelPair;
use crate::models::user::CocoUser;
use std::collections::HashMap;

#[derive(Serialize)]
pub struct CocoLogResponse {
    pub user: Option<CocoUser>,
    pub message: String,
    pub timestamp: i64,
}

#[derive(Serialize)]
pub struct CocoFullLogResponse {
    pub pair: CocoChannelPair,
    pub logs: Vec<CocoLogResponse>,
}

async fn fetch_logs(srv: &CocoServer, pair: i64) -> anyhow::Result<Vec<CocoLogResponse>> {
    let mut logs = Vec::new();
    let mut users = HashMap::<i64, Option<CocoUser>>::new();
    for raw in CocoLog::find(srv, pair).await? {
        let user = if let Some(usr) = users.get(&raw.user) {
            usr.clone()
        } else {
            let usr = CocoUser::get(srv, raw.user).await?;
            users.insert(raw.user, usr.clone());
            usr
        };
        let log = CocoLogResponse {
            user,
            message: raw.message,
            timestamp: raw.timestamp,
        };
        logs.push(log);
    }
    Ok(logs)
}

async fn offloader(
    srv: &CocoServer,
    token: impl ToString,
    pair: i64,
) -> anyhow::Result<warp::http::Response<String>> {
    Ok(if let Some(auth) = CocoAuth::get(srv, token).await? {
        if let Some(user) = CocoUser::get(srv, auth.id).await? {
            if let Some(pair) = CocoChannelPair::get(srv, user.id(), pair).await? {
                let logs = fetch_logs(srv, pair.id).await?;
                let full = CocoFullLogResponse { pair, logs };
                warp::http::Response::builder()
                    .status(200)
                    .header("Content-Type", "application/json")
                    .body(serde_json::to_string(&full)?)?
            } else {
                warp::http::Response::builder()
                    .status(404)
                    .body("Channel pair not found.".to_string())?
            }
        } else {
            warp::http::Response::builder()
                .status(403)
                .body("Forbidden.".to_string())?
        }
    } else {
        warp::http::Response::builder()
            .status(401)
            .body("Unauthorized.".to_string())?
    })
}

pub async fn log_list_handler(
    srv: CocoServer,
    token: String,
    pair: i64,
) -> Result<impl Reply, Rejection> {
    Ok(match offloader(&srv, token, pair).await {
        Ok(reply) => reply,
        Err(err) => warp::http::Response::builder()
            .status(500)
            .body(format!("Error: {}", err))
            .unwrap(),
    })
}
