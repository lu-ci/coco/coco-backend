use warp::{Rejection, Reply};

use crate::core::server::CocoServer;
use crate::models::auth::CocoAuth;
use crate::models::channel::CocoChannel;
use crate::models::user::CocoUser;

async fn offloader(
    srv: &CocoServer,
    token: impl ToString,
    uri: String,
) -> anyhow::Result<warp::http::Response<String>> {
    Ok(if let Some(auth) = CocoAuth::get(srv, token).await? {
        if CocoUser::get(srv, auth.id).await?.is_some() {
            let chn = CocoChannel::from_uri(&uri)?;
            chn.test().await?;
            warp::http::Response::builder()
                .status(201)
                .header("Content-Type", "application/json")
                .body("".to_owned())?
        } else {
            warp::http::Response::builder()
                .status(403)
                .body("Forbidden.".to_string())?
        }
    } else {
        warp::http::Response::builder()
            .status(401)
            .body("Unauthorized.".to_string())?
    })
}

pub async fn hook_test_handler(
    srv: CocoServer,
    token: String,
    uri: impl ToString,
) -> Result<impl Reply, Rejection> {
    let uri = uri.to_string();
    Ok(match offloader(&srv, token, uri).await {
        Ok(reply) => reply,
        Err(err) => warp::http::Response::builder()
            .status(500)
            .body(format!("Error: {}", err))
            .unwrap(),
    })
}
