use crate::core::server::CocoServer;
use crate::models::auth::CocoAuth;
use crate::models::channel::CocoChannel;
use crate::models::log::CocoLog;
use crate::models::pair::{CocoChanelPairRequest, CocoChannelPair, CocoChannelPairResponse};
use crate::models::user::CocoUser;
use warp::{Rejection, Reply};

async fn offloader(
    srv: &CocoServer,
    token: impl ToString,
    pair: i64,
    data: CocoChanelPairRequest,
) -> anyhow::Result<warp::http::Response<String>> {
    Ok(if let Some(auth) = CocoAuth::get(srv, token).await? {
        if let Some(user) = CocoUser::get(srv, auth.id).await? {
            if let Some(pair) = CocoChannelPair::get(srv, user.id(), pair).await? {
                let data = data.to_normal()?;
                let prest = CocoChannelPairResponse::from_pair(srv, &pair).await?;
                let sfw_valid = if let Some(sfw) = data.sfw.clone() {
                    CocoChannel::from(sfw).validate().await.is_ok()
                } else {
                    true
                };
                let nsfw_valid = if let Some(nsfw) = data.nsfw.clone() {
                    CocoChannel::from(nsfw).validate().await.is_ok()
                } else {
                    true
                };
                let same_channel = if let (Some(sfw), Some(nsfw)) = (&prest.sfw, &prest.nsfw) {
                    sfw.id == nsfw.id
                } else {
                    false
                };
                if sfw_valid && nsfw_valid {
                    if prest.sfw.is_none() && data.sfw.is_some() {
                        let chn_resp = data.sfw.clone().unwrap();
                        let chn = CocoChannel {
                            id: chn_resp.id,
                            token: chn_resp.token,
                        };
                        chn.save(srv).await?;
                    }
                    if prest.nsfw.is_none() && data.nsfw.is_some() {
                        let chn_resp = data.nsfw.clone().unwrap();
                        let chn = CocoChannel {
                            id: chn_resp.id,
                            token: chn_resp.token,
                        };
                        chn.save(srv).await?;
                    }
                    if prest.sfw.is_some() && data.sfw.is_none() {
                        let chn_resp = prest.sfw.clone().unwrap();
                        let chn = CocoChannel {
                            id: chn_resp.id,
                            token: chn_resp.token,
                        };
                        if !same_channel {
                            chn.delete(srv).await?;
                        }
                    }
                    if prest.nsfw.is_some() && data.nsfw.is_none() {
                        let chn_resp = prest.nsfw.clone().unwrap();
                        let chn = CocoChannel {
                            id: chn_resp.id,
                            token: chn_resp.token,
                        };
                        if !same_channel {
                            chn.delete(srv).await?;
                        }
                    }
                    if let (Some(prest), Some(data)) = (&prest.sfw, &data.sfw) {
                        let prest_chn = CocoChannel::from(prest);
                        let data_chn = CocoChannel::from(data);
                        if prest_chn != data_chn {
                            prest_chn.delete(srv).await?;
                            data_chn.save(srv).await?;
                        }
                    }
                    if let (Some(prest), Some(data)) = (&prest.nsfw, &data.nsfw) {
                        let prest_chn = CocoChannel::from(prest);
                        let data_chn = CocoChannel::from(data);
                        if prest_chn != data_chn {
                            prest_chn.delete(srv).await?;
                            data_chn.save(srv).await?;
                        }
                    }
                    let pair = CocoChannelPair::from(data.clone());
                    pair.save(srv).await?;
                    let _ = CocoLog::create(srv, user.id(), pair.id, "edited").await?;
                    warp::http::Response::builder()
                        .status(200)
                        .header("Content-Type", "application/json")
                        .body(serde_json::to_string(&data)?)?
                } else {
                    warp::http::Response::builder()
                        .status(400)
                        .body("One of the webhooks isn't valid.".to_string())?
                }
            } else {
                warp::http::Response::builder()
                    .status(404)
                    .body("Channel pair not found.".to_string())?
            }
        } else {
            warp::http::Response::builder()
                .status(403)
                .body("Forbidden.".to_string())?
        }
    } else {
        warp::http::Response::builder()
            .status(401)
            .body("Unauthorized.".to_string())?
    })
}

pub async fn pair_update_handler(
    srv: CocoServer,
    token: String,
    pair: i64,
    data: CocoChanelPairRequest,
) -> Result<impl Reply, Rejection> {
    Ok(match offloader(&srv, token, pair, data).await {
        Ok(reply) => reply,
        Err(err) => warp::http::Response::builder()
            .status(500)
            .body(format!("Error: {}", err))
            .unwrap(),
    })
}
