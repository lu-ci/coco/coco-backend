use crate::core::server::CocoServer;
use crate::models::auth::CocoAuth;
use crate::models::pair::CocoChannelPair;
use crate::models::user::CocoUser;
use warp::{Rejection, Reply};

async fn offloader(
    srv: &CocoServer,
    token: impl ToString,
    pair: i64,
) -> anyhow::Result<warp::http::Response<String>> {
    Ok(if let Some(auth) = CocoAuth::get(srv, token).await? {
        if let Some(user) = CocoUser::get(srv, auth.id).await? {
            if let Some(pair) = CocoChannelPair::delete(srv, user.id(), pair).await? {
                warp::http::Response::builder()
                    .status(200)
                    .header("Content-Type", "application/json")
                    .body(serde_json::to_string(&pair)?)?
            } else {
                warp::http::Response::builder()
                    .status(404)
                    .body("Channel pair not found.".to_string())?
            }
        } else {
            warp::http::Response::builder()
                .status(403)
                .body("Forbidden.".to_string())?
        }
    } else {
        warp::http::Response::builder()
            .status(401)
            .body("Unauthorized.".to_string())?
    })
}

pub async fn pair_delete_handler(
    srv: CocoServer,
    token: String,
    pair: i64,
) -> Result<impl Reply, Rejection> {
    Ok(match offloader(&srv, token, pair).await {
        Ok(reply) => reply,
        Err(err) => warp::http::Response::builder()
            .status(500)
            .body(format!("Error: {}", err))
            .unwrap(),
    })
}
