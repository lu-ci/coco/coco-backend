use crate::core::server::CocoServer;
use crate::models::auth::CocoAuth;
use warp::{Rejection, Reply};

async fn offloader(
    srv: &CocoServer,
    token: impl ToString,
    code: impl ToString,
) -> anyhow::Result<warp::http::Response<String>> {
    let auth = CocoAuth::oauth(srv, token, code).await?;
    Ok(warp::http::Response::builder()
        .status(201)
        .header("Content-Type", "application/json")
        .body(serde_json::to_string(&auth)?)?)
}

pub async fn user_auth_handler(
    srv: CocoServer,
    token: String,
    code: String,
) -> Result<impl Reply, Rejection> {
    Ok(match offloader(&srv, token, code).await {
        Ok(reply) => reply,
        Err(err) => warp::http::Response::builder()
            .status(500)
            .body(format!("Error: {}", err))
            .unwrap(),
    })
}
