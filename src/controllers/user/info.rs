use warp::{Rejection, Reply};

use crate::core::server::CocoServer;
use crate::models::auth::CocoAuth;
use crate::models::user::CocoUser;

async fn offloader(
    srv: &CocoServer,
    token: impl ToString,
) -> anyhow::Result<warp::http::Response<String>> {
    Ok(if let Some(auth) = CocoAuth::get(srv, token).await? {
        if let Some(user) = CocoUser::get(srv, auth.id).await? {
            warp::http::Response::builder()
                .status(200)
                .header("Content-Type", "application/json")
                .body(serde_json::to_string(&user)?)?
        } else {
            warp::http::Response::builder()
                .status(403)
                .body("Forbidden.".to_string())?
        }
    } else {
        warp::http::Response::builder()
            .status(401)
            .body("Unauthorized.".to_string())?
    })
}

pub async fn user_info_handler(srv: CocoServer, token: String) -> Result<impl Reply, Rejection> {
    Ok(match offloader(&srv, token).await {
        Ok(reply) => reply,
        Err(err) => warp::http::Response::builder()
            .status(500)
            .body(format!("Error: {}", err))
            .unwrap(),
    })
}
