use std::collections::HashMap;
use std::convert::Infallible;

use warp::Filter;

use crate::controllers::logs::log_list_handler;
use crate::controllers::pair::create::pair_create_handler;
use crate::controllers::pair::delete::pair_delete_handler;
use crate::controllers::pair::index::pair_index_handler;
use crate::controllers::pair::read::pair_read_handler;
use crate::controllers::pair::update::pair_update_handler;
use crate::controllers::test::hook::hook_test_handler;
use crate::controllers::tracks::track_list_handler;
use crate::controllers::user::auth::user_auth_handler;
use crate::controllers::user::info::user_info_handler;
use crate::core::config::CocoConfig;
use crate::core::database::CocoDatabase;
use crate::models::pair::CocoChanelPairRequest;

const AUTH_HEADER: &str = "Authorization";

#[derive(Clone)]
pub struct CocoServer {
    pub cfg: CocoConfig,
    pub db: CocoDatabase,
}

impl CocoServer {
    pub async fn new() -> anyhow::Result<Self> {
        let cfg = CocoConfig::new()?;
        let db = CocoDatabase::new(&cfg).await?;
        Ok(Self { cfg, db })
    }

    fn with_core(srv: Self) -> impl warp::Filter<Extract = (Self,), Error = Infallible> + Clone {
        warp::any().map(move || srv.clone())
    }

    pub async fn run() -> anyhow::Result<()> {
        let srv = Self::new().await?;
        println!("Starting the backend on {} ...", srv.cfg.core.get_uri());
        let host = srv.cfg.core.get_host()?;
        let oauth = warp::any().and(
            warp::path("api").and(
                warp::path("v1").and(
                    warp::path("oauth")
                        .and(warp::filters::method::post())
                        .and(Self::with_core(srv.clone()))
                        .and(warp::filters::header::header(AUTH_HEADER))
                        .and(warp::filters::body::form().and_then(
                            |form: HashMap<String, String>| async move {
                                if let Some(code) = form.get("code") {
                                    Ok(code.to_string())
                                } else {
                                    Err(warp::reject())
                                }
                            },
                        ))
                        .and_then(user_auth_handler)
                        .or(warp::path("user")
                            .and(warp::filters::method::get())
                            .and(Self::with_core(srv.clone()))
                            .and(warp::filters::header::header(AUTH_HEADER))
                            .and_then(user_info_handler))
                        .or(warp::path("pair").and(
                            warp::filters::method::post()
                                .and(Self::with_core(srv.clone()))
                                .and(warp::filters::header::header(AUTH_HEADER))
                                .and_then(pair_create_handler)
                                .or(warp::filters::method::get()
                                    .and(Self::with_core(srv.clone()))
                                    .and(warp::filters::header::header(AUTH_HEADER))
                                    .and(warp::path::param::<i64>())
                                    .and_then(pair_read_handler))
                                .or(warp::filters::method::get()
                                    .and(Self::with_core(srv.clone()))
                                    .and(warp::filters::header::header(AUTH_HEADER))
                                    .and_then(pair_index_handler))
                                .or(warp::filters::method::delete()
                                    .and(Self::with_core(srv.clone()))
                                    .and(warp::filters::header::header(AUTH_HEADER))
                                    .and(warp::path::param::<i64>())
                                    .and_then(pair_delete_handler))
                                .or(warp::filters::method::put()
                                    .and(Self::with_core(srv.clone()))
                                    .and(warp::filters::header::header(AUTH_HEADER))
                                    .and(warp::path::param::<i64>())
                                    .and(warp::filters::body::json::<CocoChanelPairRequest>())
                                    .and_then(pair_update_handler)),
                        ))
                        .or(warp::path("logs")
                            .and(warp::filters::method::get())
                            .and(Self::with_core(srv.clone()))
                            .and(warp::filters::header::header(AUTH_HEADER))
                            .and(warp::path::param::<i64>())
                            .and_then(log_list_handler))
                        .or(warp::path("tracks")
                            .and(warp::filters::method::get())
                            .and(Self::with_core(srv.clone()))
                            .and(warp::filters::header::header(AUTH_HEADER))
                            .and(warp::path::param::<i64>())
                            .and_then(track_list_handler))
                        .or(warp::path("test")
                            .and(warp::filters::method::post())
                            .and(Self::with_core(srv.clone()))
                            .and(warp::filters::header::header(AUTH_HEADER))
                            .and(warp::filters::body::form().and_then(
                                |form: HashMap<String, String>| async move {
                                    if let Some(code) = form.get("uri") {
                                        Ok(code.to_string())
                                    } else {
                                        Err(warp::reject())
                                    }
                                },
                            ))
                            .and_then(hook_test_handler)),
                ),
            ),
        );
        warp::serve(oauth).bind(host).await;
        Ok(())
    }
}
