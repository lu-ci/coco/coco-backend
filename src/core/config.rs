use std::net::{Ipv4Addr, SocketAddrV4};
use std::str::FromStr;

use serde::Deserialize;

#[derive(Clone)]
pub struct CocoConfig {
    pub core: CocoCoreConfig,
    pub db: CocoDatabaseConfig,
    pub dsc: CocoDiscordConfig,
}

impl CocoConfig {
    pub fn new() -> anyhow::Result<Self> {
        Ok(Self {
            core: CocoCoreConfig::new()?,
            db: CocoDatabaseConfig::new()?,
            dsc: CocoDiscordConfig::new()?,
        })
    }
}

#[derive(Clone, Deserialize)]
pub struct CocoCoreConfig {
    pub host: String,
    pub port: u16,
    pub debug: bool,
    pub redirect: String,
}

impl Default for CocoCoreConfig {
    fn default() -> Self {
        Self {
            host: "127.0.0.1".to_string(),
            port: 3030,
            debug: false,
            redirect: "http://127.0.0.1/discord".to_string(),
        }
    }
}

impl CocoCoreConfig {
    pub fn new() -> anyhow::Result<Self> {
        let path = std::path::Path::new("config/core.yml");
        if path.exists() {
            let content = std::fs::read_to_string(&path)?;
            let cfg = serde_yaml::from_str::<Self>(&content)?;
            let _ = cfg.get_host()?;
            Ok(cfg)
        } else {
            Ok(Self::default())
        }
    }

    pub fn get_host(&self) -> anyhow::Result<SocketAddrV4> {
        Ok(SocketAddrV4::new(
            Ipv4Addr::from_str(&self.host)?,
            self.port,
        ))
    }

    pub fn get_uri(&self) -> String {
        format!("http://{}:{}/", self.host, self.port)
    }
}

#[derive(Clone, Deserialize)]
pub struct CocoDatabaseConfig {
    pub host: String,
    pub port: u16,
    pub username: Option<String>,
    pub password: Option<String>,
    pub database: String,
}

impl Default for CocoDatabaseConfig {
    fn default() -> Self {
        Self {
            host: "127.0.0.1".to_string(),
            port: 27017,
            username: None,
            password: None,
            database: "coco".to_string(),
        }
    }
}

impl CocoDatabaseConfig {
    pub fn new() -> anyhow::Result<Self> {
        let path = std::path::Path::new("config/database.yml");
        if path.exists() {
            let content = std::fs::read_to_string(&path)?;
            let cfg = serde_yaml::from_str::<Self>(&content)?;
            Ok(cfg)
        } else {
            Ok(Self::default())
        }
    }

    pub fn get_uri(&self) -> String {
        if self.username.is_some() && self.password.is_some() {
            let username = self.username.clone().unwrap();
            let password = self.password.clone().unwrap();
            format!(
                "mongodb://{}:{}@{}:{}/",
                username, password, &self.host, self.port
            )
        } else {
            format!("mongodb://{}:{}/", &self.host, self.port)
        }
    }
}

#[derive(Clone, Deserialize)]
pub struct CocoDiscordConfig {
    pub id: i64,
    pub secret: String,
}

impl CocoDiscordConfig {
    pub fn new() -> anyhow::Result<Self> {
        let path = std::path::Path::new("config/discord.yml");
        if path.exists() {
            let content = std::fs::read_to_string(&path)?;
            let cfg = serde_yaml::from_str::<Self>(&content)?;
            Ok(cfg)
        } else {
            Err(anyhow::Error::msg("Discord configuration is missing."))
        }
    }
}
