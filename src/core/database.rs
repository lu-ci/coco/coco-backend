use crate::core::config::{CocoConfig, CocoDatabaseConfig};

#[derive(Clone)]
pub struct CocoDatabase {
    pub cfg: CocoDatabaseConfig,
    pub cli: mongodb::Client,
}

impl CocoDatabase {
    pub async fn new(cfg: &CocoConfig) -> anyhow::Result<Self> {
        let opts = mongodb::options::ClientOptions::parse(cfg.db.get_uri()).await?;
        let cli = mongodb::Client::with_options(opts)?;
        Ok(Self {
            cfg: cfg.db.clone(),
            cli,
        })
    }

    pub fn db(&self) -> mongodb::Database {
        self.cli.database(&self.cfg.database)
    }

    pub fn coll(&self, name: impl ToString) -> mongodb::Collection<bson::Document> {
        let name = name.to_string();
        self.db().collection(&name)
    }
}
