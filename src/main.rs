use crate::core::server::CocoServer;

mod controllers;
mod core;
mod models;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    CocoServer::run().await?;
    Ok(())
}
